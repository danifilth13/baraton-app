1. Instalar en la carpeta raíz del proyecto: npm y Bower teniendo encuenta la instalación previa de NodeJs :
npm install npm@latest -g
npm install -g bower

2. Ejecutar bower install bower.json

3. dirijirse a : bower_components\ng-bootstrap-submenu\dist y modificar el archivo: ng-bootstrap-submenu.js en la linea 79 
reemplazar linea completa por:
angular.module("bootstrapSubmenu").run(["$templateCache", function($templateCache) {$templateCache.put("bootstrapSubmenu.html","<li ng-class=\"getDropdownClass()\">\r\n  <a ng-if=\"!hasChildren()\" tabindex=\"0\" ui-sref=\"main.watch({key: 'sublevel_id', value:menuItem.id })\">{{menuItem.display}}</a>\r\n  <a ng-if=\"hasChildren()\" tabindex=\"0\" ng-attr-data-toggle=\"{{ showCaret() ? \'dropdown\' : undefined }}\" ng-attr-data-submenu=\"{{ showCaret() ? \'\' : undefined }}\">\r\n     {{menuItem.display}}<span ng-if=\"showCaret()\" class=\"caret\"></span>\r\n   </a>\r\n  <ul ng-if=\"hasChildren()\" class=\"dropdown-menu\">\r\n    <bootstrap-submenu ng-repeat=\"child in menuItem.children\" menu-item=\"child\" is-sub-menu=\"true\">\r\n    </bootstrap-submenu>\r\n  </ul>\r\n</li>");}]);

4. La aplicación debería mostrarse : http://localhost:8080/baraton-app/
---------------------------------------------------------------------------------
Sugerencia:

Realizada la secuencia del carro de compra, en el checkout aparecerá un formulario en un popup emulando un sistema de pago;
para realizarlo correctamente clickear en modo Test o colocar el número de tarjeta lo siguiente : 4242424242424242,
y para la fecha colocar mes y año actuales, el resto de datos como desee. Una vez dispuestos los datos culminará el proceso 
de emulación de compra satisfactoriamente.