(function () {
    'use strict';
    
    angular
        .module('cart', [
            'cart.checkout'
        ])
        .config(config); 

    config.$inject = ['$stateProvider', 'StripeCheckoutProvider'];
    function config($stateProvider, StripeCheckoutProvider) {
 
        $stateProvider
            .state('main.cart', {
                url: 'cart',
                templateUrl: '../ecommerce-app-master/views/cart/cart.html'
            });
    }
})();
 