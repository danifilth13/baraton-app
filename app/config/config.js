/*angular.module("config", [])
.constant("BUCKET_SLUG", "ecommerce-app")
.constant("MEDIA_URL", "../ecommerce-app-master/products.json")
.constant("URL", "../ecommerce-app-master/products.json")
.constant("READ_KEY", "")
.constant("WRITE_KEY", "")
.constant("STRIPE_KEY", "pk_test_oRv6WcnATRyMqponKKG6QlON");
*/

angular.module("config", [])
.constant("BUCKET_SLUG", "ecommerce-app")
.constant("MEDIA_URL", "https://api.cosmicjs.com/v1/ecommerce-app/media")
.constant("URL", "https://api.cosmicjs.com/v1/")
.constant("READ_KEY", "")
.constant("WRITE_KEY", "")
.constant("STRIPE_KEY", "pk_test_oRv6WcnATRyMqponKKG6QlON");
